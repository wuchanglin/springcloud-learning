package com.forest.serverregistcenter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class ServerRegistCenterApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerRegistCenterApplication.class, args);
	}
}
