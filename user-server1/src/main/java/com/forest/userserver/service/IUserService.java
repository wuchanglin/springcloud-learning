package com.forest.userserver.service;

import com.forest.userserver.entity.User;

/**
 * @author phwucl
 * @desp 类说明：
 * @since 2018/5/21 15:23
 */
public interface IUserService {
    User getUserById(String id);
}
