package com.forest.userserver.service.impl;

import com.forest.userserver.entity.User;
import com.forest.userserver.service.IUserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * @author phwucl
 * @desp 类说明：
 * @since 2018/5/21 15:24
 */
@Service
public class UserService implements IUserService {

    @Value("${env}")
    private String evn;
    @Override
    public User getUserById(String id) {
        User u1 = new User(id,evn,22);
        return u1;
    }
}
