package com.forest.orderserver.config;

import com.forest.userserver.entity.User;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author phwucl
 * @desp 类说明：
 * @since 2018/5/24 14:36
 */
@FeignClient("user-server")
public interface UserServerClient {

    @RequestMapping(value="/getUser" ,method = RequestMethod.GET)
    User getUser();
}
