package com.forest.orderserver.config;

import feign.Retryer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static java.util.concurrent.TimeUnit.SECONDS;

/**
 * @author phwucl
 * @desp 类说明：
 * @since 2018/6/22 15:21
 */
@Configuration
public class FeignConfiguration {

    @Bean
    public Retryer feignRetryer(){
        return new Retryer.Default(100,SECONDS.toMillis(1),5);
    }
}
