package com.forest.orderserver.controller;

import com.forest.orderserver.config.UserServerClient;
import com.forest.userserver.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author phwucl
 * @desp 类说明：
 * @since 2018/5/21 16:43
 */
@RestController
public class OrderController {

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private UserServerClient userServerClient;

    @GetMapping("/user/id")
    public User getUser(){
        return this.userServerClient.getUser();
    }

     /*@Autowired
    private RestTemplate restTemplate;

    @GetMapping("/")
    public User getInfo(){
        //return this.restTemplate.getForObject("http://localhost:8081/info/"+id,User.class);
        return this.restTemplate.getForEntity("http://user-server",User.class).getBody();
    }*/
}
