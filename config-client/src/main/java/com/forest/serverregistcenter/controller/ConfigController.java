package com.forest.serverregistcenter.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author phwucl
 * @desp 类说明：
 * @since 2018/6/13 11:10
 */
@RestController
public class ConfigController {

    @Value("${topic}")
    private String profile;
    @GetMapping("/profile")
    public String getProfile(){
        return profile;
    }
}
