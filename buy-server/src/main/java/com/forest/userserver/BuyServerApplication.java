package com.forest.userserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
public class BuyServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BuyServerApplication.class, args);
	}
}
