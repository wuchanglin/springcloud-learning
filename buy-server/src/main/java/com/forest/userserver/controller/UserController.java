package com.forest.userserver.controller;

import com.forest.userserver.entity.User;
import com.forest.userserver.service.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author phwucl
 * @desp 类说明：
 * @since 2018/5/21 15:20
 */
@RestController
public class UserController {

    @SuppressWarnings("SpringJavaAutowiringInspection")
    @Autowired
    private IUserService userService;

    @GetMapping("/getUser")
    public User getUserById(){
        User user = userService.getUserById();
        return user;
    }
}
