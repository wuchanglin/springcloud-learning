package com.forest.orderserver.config;

import com.forest.configration.Myconfigration;
import com.forest.userserver.entity.User;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * @author phwucl
 * @desp 类说明：
 * @since 2018/5/24 14:36
 */
@FeignClient(name = "user-server",configuration = Myconfigration.class)
public interface UserServerClient {

    @RequestLine("GET /")
    //@RequestMapping(value = "/" ,method = RequestMethod.GET)
    User getUser();
}
