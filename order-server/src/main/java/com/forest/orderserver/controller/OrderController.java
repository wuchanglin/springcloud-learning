package com.forest.orderserver.controller;

import com.forest.userserver.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @author phwucl
 * @desp 类说明：
 * @since 2018/5/21 16:43
 */
@RestController
public class OrderController {

    @Autowired
    private RestTemplate restTemplate;
    @GetMapping("/")
    public User getInfo(){
        //return this.restTemplate.getForObject("http://localhost:8081/info/"+id,User.class);
        return this.restTemplate.getForEntity("http://user-server",User.class).getBody();
    }
}
